# Resume Submit ( Example )

By using this module user can able to upload a resume.

Admin can able to manage uploaded resume listing. and filter.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/resume_submit).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/resume_submit).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.


## Maintainers

- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
