<?php

namespace Drupal\resume\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Forms.
 */
class ResumeForm extends FormBase {
	use MessengerTrait;
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resume_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['candidate_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Candidate Name:'),
      '#required' => TRUE,
    ];

    $form['candidate_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email ID:'),
      '#required' => TRUE,
    ];

    $form['candidate_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Mobile no'),
    ];

    $form['candidate_dob'] = [
      '#type' => 'date',
      '#title' => $this->t('DOB'),
      '#required' => TRUE,
    ];

    $form['candidate_gender'] = [
      '#type' => 'select',
      '#title' => ('Gender'),
      '#options' => [
        'Female' => $this->t('Female'),
        'male' => $this->t('Male'),
      ],
    ];

    $form['candidate_confirmation'] = [
      '#type' => 'radios',
      '#title' => ('Are you above 18 years old?'),
      '#options' => [
        'Yes' => $this->t('Yes'),
        'No' => $this->t('No'),
      ],
    ];

    $form['candidate_copy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send me a copy of the application.'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (strlen($form_state->getValue('candidate_number')) < 10) {
      $form_state->setErrorByName('candidate_number', $this->t('Mobile number is too short.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // drupal_set_message($this->t('@can_name ,Your application is being
    // submitted!',
    // array('@can_name' => $form_state->getValue('candidate_name'))));.
    foreach ($form_state->getValues() as $key => $value) {
      //drupal_set_message($key . ': ' . $value);
	  $this->messenger()->addMessage($key . ': ' . $value);
    }

  }

}